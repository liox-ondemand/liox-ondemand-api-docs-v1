=========================
Libraries and Sample Code
=========================

PHP developers will find `this sample code by Andrey Artiushenko <https://github.com/workaandrey/ondemand-php-sdk>`_  helpful. You can import it into your project and use it as is or just borrow the bits that deal with authenticating requests.

`Brian Kelly <http://spilth.org>`_ has developed a `Command Line Interface (CLI) for the Lionbridge Content API <http://developers.lionbridge.com/content-cli/docs/>`_. This is the easiest way to access translation services with your scripted automations.

`Dan Spector <https://www.linkedin.com/in/danspector03>`_ has written an `onDemand API client library in C# <https://bitbucket.org/liox-ondemand/liox-content-sdk-csharp>`_ for .NET developers (`NuGet <https://www.nuget.org/packages/Lionbridge.onDemand.Client>`_). This library is very actively used in the `Clay Tablet <http://clay-tablet.com>`_ connector for onDemand.

`Quoin <https://www.quoininc.com/>`_ has written an `onDemand API client library in Java <http://developers.lionbridge.com/content/jsdk-docs/>`_. This library is very actively used in the `Lionbridge onDemand <http://www.lionbridge.com>`_ connector for `Adobe Experence Manager <http://www.adobe.com/marketing-cloud/enterprise-content-management.html>`_.
