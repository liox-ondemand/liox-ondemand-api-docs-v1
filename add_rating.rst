===================
Add Rating
===================

+-----------------+---------------------------------------------------------+
| **Resource:**   | .. container:: notrans                                  |
|                 |                                                         |
|                 |    /api/project/<<project_id>>/<<language_code>/rating  |
+-----------------+---------------------------------------------------------+
| **Method:**     | .. container:: notrans                                  |
|                 |                                                         |
|                 |    POST                                                 |
+-----------------+---------------------------------------------------------+

This interface adds a rating to a translation.

Arguments
=========

The URL for this method includes two parameters.  

- **Project ID:** The onDemand Project ID.  You will receive this ID from :doc:`generate_quote`
- **language_code** is a locale code (e.g. ``en-gb`` where ``en`` is the two-letter ISO language code for English and ``gb`` is the two-letter ISO country code for Great Britain). This language code refers to the .

Request Body
============

+-------------------------+-------------------------+--------------------------------+
| Parameter               | Type                    | Comments                       |
+=========================+=========================+================================+
| .. container:: notrans  | Integer                 | Value of the rating            |
|                         |                         |                                |
|    Score                |                         | from 1-5.                      |
|                         |                         |                                |
+-------------------------+-------------------------+--------------------------------+
| .. container:: notrans  | String                  | An optional feedback value for |
|                         |                         |                                |
|    CustomerFeedback     |                         | the rating. This field becomes |
|                         |                         |                                |
|                         |                         | required if the Score is       | 
|                         |                         |                                |
|                         |                         | either 1 or 2.                 |
|                         |                         |                                |
+-------------------------+-------------------------+--------------------------------+



Return Codes
============


+-------------------------+-------------------------+-----------------------------------------+
| Status                  | Code                    | Comments                                |
+=========================+=========================+=========================================+
| Created                 | 200                     | The rating was successfully created.    |
|                         |                         |                                         |
+-------------------------+-------------------------+-----------------------------------------+
| Not Found               | 404                     | The URL does not relate to a project    |
|                         |                         |                                         |
|                         |                         | that the account owns.                  |
|                         |                         |                                         |
+-------------------------+-------------------------+-----------------------------------------+
| Conflict                | 409                     | The request did not pass the validation |
|                         |                         |                                         |
|                         |                         | requirement to create the rating.       |
|                         |                         |                                         |
+-------------------------+-------------------------+-----------------------------------------+

Response Body
=============

+-------------------------+-------------------------+--------------------------------+
| Parameter               | Type                    | Comments                       |
+=========================+=========================+================================+
| .. container:: notrans  | Integer                 | Value of the rating from 1-5.  |
|                         |                         |                                |
|    Score                |                         | 1 - Unacceptable               |
|                         |                         |                                |
|                         |                         | 2 - Poor                       |
|                         |                         |                                |
|                         |                         | 3 - Good Enough                |
|                         |                         |                                |
|                         |                         | 4 - Very Good                  |
|                         |                         |                                |
|                         |                         | 5 - Excellent                  |
|                         |                         |                                |
+-------------------------+-------------------------+--------------------------------+
| .. container:: notrans  | String                  | An optional feedback value for |
|                         |                         |                                |
|    CustomerFeedback     |                         | the rating. This field becomes |
|                         |                         |                                |
|                         |                         | required if the Score is       | 
|                         |                         |                                |
|                         |                         | either 1 or 2.                 |
|                         |                         |                                |
+-------------------------+-------------------------+--------------------------------+
| .. container:: notrans  | String                  | String representing the        |
|                         |                         |                                |
|    RatingDate           |                         | date/time in the ISO           |
|                         |                         |                                |
|                         |                         | 8601 format. that the          | 
|                         |                         |                                |
|                         |                         | rating was created in UTC.     |
|                         |                         |                                |
+-------------------------+-------------------------+--------------------------------+
| .. container:: notrans  | String                  | Account name who made          |
|                         |                         |                                |
|    RatedBy              |                         | the rating.                    |
|                         |                         |                                |
+-------------------------+-------------------------+--------------------------------+



Request Example
================================

::

    <TranslationRating>
        <Score>5</Score>
        <CustomerFeedback>Excellent!</CustomerFeedback>
    </TranslationRating>


Response Example
======================================

::

    <TranslationRating>
        <Score>5</Score>
        <CustomerFeedback>Excellent!</CustomerFeedback>
        <RatingDate>2017-04-04T18:12:58.959249Z</RatingDate>
        <RatedBy>John Doe</RatedBy>
    </TranslationRating>




Errors
======
If add rating encountered an error, the response will contain an Error element consisting of
a ReasonCode, SimpleMessage, and DetailedMessage elements. See :doc:`error_handling` for more
information. Here are some common cases.

+-------------------------+-------------------------+-------------------------+
| ReasonCode              | SimpleMessage           | DetailedMessage         |
+=========================+=========================+=========================+
| 301                     | No Score.               | Score is                |
|                         |                         |                         |
|                         |                         | required in the Request.|
|                         |                         |                         |
+-------------------------+-------------------------+-------------------------+
| 302                     | No CustomerFeedback.    | CustomerFeedback is     |
|                         |                         |                         |
|                         |                         | required in the Request.|
|                         |                         |                         |
+-------------------------+-------------------------+-------------------------+
| 303                     | Invalid score value.    | Score value should be   |
|                         |                         |                         |
|                         |                         | between 1 and 5.        |
|                         |                         |                         |
+-------------------------+-------------------------+-------------------------+
