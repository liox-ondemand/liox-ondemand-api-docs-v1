==========================
Notification Subscriptions
==========================

If the quote creation request included NotificationSubscriptions, notifications
will be sent to the supplied endpoints.

Options
=======

Notification Subscriptions support different methods for how notifications will
be sent. The desired method is determined by what is supplied in the :code:`<Endpoint>`
element. We currently support POST and Email based notifications. Not all events
will support every method, however.

POST Notification Example
=========================

If the endpoint of the subscription begins with :code:`http://` or :code:`https://`, a POST
request will be sent to the supplied endpoint when the event occurs.

::

    ...
    <NotificationSubscriptions>
        <NotificationSubscription>
            <Endpoint>http://www.test.com</Endpoint>
            <EventName>quote-ready</EventName>
        </NotificationSubscription>
        <NotificationSubscription>
            <Endpoint>https://www.test.com</Endpoint>
            <EventName>quote-paid</EventName>
        </NotificationSubscription>
    </NotificationSubscriptions>
    ...


Email Notification Example
==========================

If the endpoint of the subscription begins with :code:`mailto:`, an email notification
will be sent to the supplied endpoint when the event occurs. To send the
notification to multiple email addresses, supply a comma separated list of email
addresses.

::

    ...
    <NotificationSubscriptions>
        <NotificationSubscription>
            <Endpoint>mailto:dev@lionbridge.com</Endpoint>
            <EventName>quote-ready</EventName>
        </NotificationSubscription>
        <NotificationSubscription>
            <Endpoint>mailto:dev1@lionbridge.com,dev2@lionbridge.com</Endpoint>
            <EventName>quote-paid</EventName>
        </NotificationSubscription>
    </NotificationSubscriptions>
    ...


Available Notification Subscription Events
==========================================

+------------------------+--------------------------+-------------------+-----------------------------------+
| Event                  | XML Value                | Methods           | Description                       |
+========================+==========================+===================+===================================+
| .. container:: notrans | quote-ready              | POST, EMAIL       | When the quote has been priced    |
|                        |                          |                   |                                   |
|    Quote Ready         |                          |                   | and is ready for payment.         |
|                        |                          |                   |                                   |
|                        |                          |                   | See :doc:`notify_quote_ready`     |   
|                        |                          |                   |                                   |
+------------------------+--------------------------+-------------------+-----------------------------------+
| .. container:: notrans | quote-paid               | POST, EMAIL       | When the quote has been paid.     |
|                        |                          |                   |                                   |
|    Quote Paid          |                          |                   | This is useful when the customer  |
|                        |                          |                   |                                   |   
|                        |                          |                   | must go to another site such as   |
|                        |                          |                   |                                   |   
|                        |                          |                   | PayPal to pay the quote balance.  |
|                        |                          |                   |                                   |
|                        |                          |                   | See :doc:`notify_quote_paid`      |
|                        |                          |                   |                                   | 
+------------------------+--------------------------+-------------------+-----------------------------------+
| .. container:: notrans | project-complete         | POST, EMAIL       | When a project within the quote   |
|                        |                          |                   |                                   |
|    Project Complete    |                          |                   | is completed.                     |
|                        |                          |                   |                                   |
|                        |                          |                   | See :doc:`notify_project_complete`|
|                        |                          |                   |                                   |
+------------------------+--------------------------+-------------------+-----------------------------------+
| .. container:: notrans | project-delay            | EMAIL             | When a project within the quote   |
|                        |                          |                   |                                   |
|    Project Delay       |                          |                   | has been delayed.                 |
|                        |                          |                   |                                   |
+------------------------+--------------------------+-------------------+-----------------------------------+
| .. container:: notrans | approved-bml-transaction | EMAIL             | Approved transaction state for the|
|                        |                          |                   |                                   |
|    Approved BML        |                          |                   | quote was paid using the "Bill me |
|                        |                          |                   |                                   |
|    Transaction         |                          |                   | Later" feature.                   |
|                        |                          |                   |                                   |
+------------------------+--------------------------+-------------------+-----------------------------------+
| .. container:: notrans | submit-for-quoting       | EMAIL             | When a quote has been sent for    |
|                        |                          |                   |                                   |
|    Submit for Quoting  |                          |                   | manual quoting.                   |
|                        |                          |                   |                                   |
+------------------------+--------------------------+-------------------+-----------------------------------+
| .. container:: notrans | quote-request-approval   | EMAIL             | When a quote has been sent for    |
|                        |                          |                   |                                   |
|    Quote Request       |                          |                   | approval.                         |
|                        |                          |                   |                                   |
|    Approval            |                          |                   |                                   |
|                        |                          |                   |                                   |
+------------------------+--------------------------+-------------------+-----------------------------------+
| .. container:: notrans | review-ready             | EMAIL             | Email to inform buyer that an     |
|                        |                          |                   |                                   |
|    Review Ready        |                          |                   | ICR review is ready.              |
|                        |                          |                   |                                   |
+------------------------+--------------------------+-------------------+-----------------------------------+
| .. container:: notrans | midway                   | EMAIL             | Email to inform buyer that a ICR  |
|                        |                          |                   |                                   |
|    Midway              |                          |                   | review deadline is halfway        |
|                        |                          |                   |                                   |
|                        |                          |                   | through.                          |
|                        |                          |                   |                                   |
+------------------------+--------------------------+-------------------+-----------------------------------+
| .. container:: notrans | trans-review-complete    | EMAIL             | Email to inform buyer that an ICR |
|                        |                          |                   |                                   |
|    Translation Review  |                          |                   | translation review is complete.   |
|                        |                          |                   |                                   |
|    Complete            |                          |                   |                                   |
|                        |                          |                   |                                   |
+------------------------+--------------------------+-------------------+-----------------------------------+
| .. container:: notrans | review-complete          | EMAIL             | Email to inform buyer that an     |
|                        |                          |                   |                                   |
|    Review Complete     |                          |                   | ICR review is complete.           |
|                        |                          |                   |                                   |
+------------------------+--------------------------+-------------------+-----------------------------------+
| .. container:: notrans | deadline-passed          | EMAIL             | Email to inform buyer that        |
|                        |                          |                   |                                   |
|    Deadline Passed     |                          |                   | the deadline has passed for an    |
|                        |                          |                   |                                   |
|                        |                          |                   | incomplete review.                |
|                        |                          |                   |                                   |
+------------------------+--------------------------+-------------------+-----------------------------------+
| .. container:: notrans | project-pending          | EMAIL             | Email to inform buyer that a      |
|                        |                          |                   |                                   |
|    Project Pending     |                          |                   | quote is being quoted.            |
|                        |                          |                   |                                   |
+------------------------+--------------------------+-------------------+-----------------------------------+
| .. container:: notrans | post-processing-failure  | EMAIL             | Email to inform buyer that a      |
|                        |                          |                   |                                   |
|    Post Processing     |                          |                   | file has failed post processing.  |
|                        |                          |                   |                                   |
|    Failure             |                          |                   |                                   |
|                        |                          |                   |                                   |
+------------------------+--------------------------+-------------------+-----------------------------------+
| .. container:: notrans | notify-owner-change      | EMAIL             | Email to inform buyer that        |
|                        |                          |                   |                                   |
|    Notify Owner Change |                          |                   | he/she has a newly assigned job.  |
|                        |                          |                   |                                   |
+------------------------+--------------------------+-------------------+-----------------------------------+
| .. container:: notrans | notify-owner-bml-order   | EMAIL             | Email to inform buyer that        |
|                        |                          |                   |                                   |
|    Notify Owner BML    |                          |                   | the purchase is being reviewed.   |
|                        |                          |                   |                                   |
|    Order               |                          |                   |                                   |
|                        |                          |                   |                                   |
+------------------------+--------------------------+-------------------+-----------------------------------+
| .. container:: notrans | cancel-project           | EMAIL             | Email quote when a quote has been |
|                        |                          |                   |                                   |
|    Cancel Project      |                          |                   | canceled.                         |
|                        |                          |                   |                                   |
+------------------------+--------------------------+-------------------+-----------------------------------+
| .. container:: notrans | send-invoice             | EMAIL             | Email to inform buyer that an     |
|                        |                          |                   |                                   |
|    Send Invoice        |                          |                   | invoice is ready.                 |
|                        |                          |                   |                                   |
+------------------------+--------------------------+-------------------+-----------------------------------+
| .. container:: notrans | retention-reminder       | EMAIL             | Email to inform buyer that jobs   |
|                        |                          |                   |                                   |
|    Retention Reminder  |                          |                   | are reaching the end of their     |
|                        |                          |                   |                                   |
|                        |                          |                   | retention period.                 |
|                        |                          |                   |                                   |
+------------------------+--------------------------+-------------------+-----------------------------------+
| .. container:: notrans | target-locale-complete   | EMAIL             | Email to inform buyer that a      |
|                        |                          |                   |                                   |
|    Target Locale       |                          |                   | translation has been completed.   |
|                        |                          |                   |                                   |
|    Complete            |                          |                   |                                   |
|                        |                          |                   |                                   |
+------------------------+--------------------------+-------------------+-----------------------------------+
