=======================================
Lionbridge onDemand API (V. 2016-03-15)
=======================================

The Lionbridge onDemand API is a RESTful programming interface to Lionbridge's onDemand Translation Service.  With the Lionbridge onDemand API you can:

- Create onDemand buyer accounts using :doc:`create_account`.
- Find the most cost-effective translation quality level for your content using :doc:`list_services`.
- Create translation projects using :doc:`generate_quote`.
- Get a notification when your project is done.


The API can be used against the `Lionbridge onDemand Retail site <https://ondemand.lionbridge.com>`_ or an `Lionbridge onDemand Enterprise <http://info.lionbridge.com/onDemand-Enterprise.html>`_ site. Scroll down for a :ref:`high_level_workflow`


**Version:** 2016-03-15 *Latest*

Earlier Releases:

- `Version 2015-02-23 <./archive/2015-02-23/>`_
- `Version 2014-09-04 <./archive/2014-09-04/>`_
- `Version 2014-06-10 <./archive/2014-06-10/>`_
- `Version 2014-02-28 <./archive/2014-02-28/>`_



New in This Release
===================

- We have a new :doc:`reject_file_translation` API. As the documentation states, this should be primarily used for cases where the translated files fail validation. It should not be used for feedback on the translaton itself. If you are not satisified with a translation, you should open a `support case <https://support.liondemand.com>`_.
- There are now two methods to "Accept" a translation. The default method is "implied." With implied, Lionbridge considers the translated file "accepted" when it is downloaded. The "acknowledge" accept method can be specified a new optional TranslationAcceptanceMethod parameter in the :doc:`generate_quote` API. With the acknowledge accept method, Lionbridge waits for a call to the :doc:`accept_file_translation` API before it considers the files accepted.
- There is another new optional parameter on the :doc:`generate_quote` API:  "SpecialInstructions" are notes to the translator.
- There is a new :doc:`request_quote_cancellation` API that can be used to cancel a quote after it has been authorized. The amount of refund (if any) will depend on how much work we have already done. Support will contact the sales owner directly to discuss the status of the cancellation.  
- The :doc:`get_quote` and :doc:`get_project` API's now display unit counts per target language.  
- We've added the ability to subscribe to various types of notification that will be sent at various points during a project's life cycle. These :doc:`notification_subscriptions` can be added when using the :doc:`generate_quote` and :doc:`add_project` API.
- There is a new :doc:`list_available_events` API that can be used to find which events and notification methods can be used when creating notification subscriptions.

Contents
========


.. toctree::
   :maxdepth: 1

   libraries.rst
   authentication
   error_handling
   create_account
   account_information
   add_prepaid_balance
   generate_quote
   reject_quote
   authorize_quote
   get_quote
   list_quotes
   notification_subscriptions
   list_available_events
   add_project
   list_projects
   get_project
   add_file
   add_file_by_reference
   list_files
   get_file
   get_file_details
   get_file_translation
   accept_file_translation
   reject_file_translation
   list_products
   get_product
   get_product_translation
   list_services
   get_service
   get_estimate
   list_locales
   get_terms
   request_quote_cancellation
   notify_quote_ready
   notify_quote_paid
   notify_project_complete
   add_rating
   get_rating
   


Getting Help
============

The Lionbridge onDemand API integration support team is eager to help you be successful with your integration.
The best way to reach them is through the `onDemand Support Portal <https://support.liondemand.com/>`_.
There you will find support articles and a form to submit a support ticket.


Getting API Keys
================

You can get API keys on our `sandbox server <https://demo.liondemand.com/>`_ by registering for an account and then visiting the `profile page <https://demo.liondemand.com/user/profile/>`_ to create your API keys.

.. image:: /_static/img/api_keys.png
   :alt: alternate text
   :align: center

If you have trouble getting your API keys or need an account on an enterprise site sandbox, please contact `support <https://support.liondemand.com/>`_.

.. _high_level_workflow:

High Level Workflow
===================


This high level sequence diagram shows the workflow for creating and completing onDemand projects.  The process starts with an end user building list of content assets to translate within the client application.  Then the merchant selects a service and target languages and requests a quote.  onDemand responds with a quote that contains information about how the work will be broken down into projects and pricing information.  The end user can authorize the quote (and if necessary pay) to start the project.


.. image:: /_static/img/high_level_workflow_sequence.png
   :alt: alternate text
   :align: center


1. The end user uses the client application to build a list of content to be translated. These assets can be products or files.
2. The end user selects an option to translate the selected assets and is presented a list of translation services that are available.  Typically, these translation services will represent different quality levels such as straight machine translation, machine translation with human post edit, crowd translation, professional translation, and specialist domain translation.  The list of avialable services can be found by calling the :doc:`list_services` API.
3. The end user submits the list of content assets for a quote.
4. If the project is file-based, the client application uploads each file using the :doc:`add_file` API.  If the project is product-based, the products are sent to onDemand when generating the quote.
5. The client application calls the :doc:`generate_quote` API to build a quote for the selected content and translation service.  Product-based projects and some file-based products will come back immediately with a completed quote containing a price. Other quotes will require additional time to parse the files.
6. If the price is not ready, the quote will come back with a status of calculating and no price.  If this is the case, the client application should regularly call the :doc:`get_quote` API until the quote is "Pending" or has a status of "Error."
7. The client application should notify the end user that the price is ready.
8. The end user can authorize or reject the quote.
9. The client application calls either the :doc:`authorize_quote` or :doc:`reject_quote` API depending on the end user's preference. If the end user has a prepaid balance or has been given free translation credit, authorizing the quote will automatically start the translation projects.  If funds are required, the end user must go through a payment process.
#. If payment is required, the authorize quote request will come back with an HTTP status code of 402.  The body of the response will contain a PaymentURL that the end user can follow to submit payment.
#. The client application sends the end user to the payment page where he or she can pay for the balance due.
#. Lionbridge will complete the projects and notify the client application that the translated content is ready.



Glossary
========

This is list of terminology used in this document.

* Currency. Lionbridge onDemand currently supports the following 4 currencies: CAD (Canadian Dollars), EUR (Euros), GBP (Great Britain Pound Sterling), USD (US Dollars).
* LanguageCode.  A locale code in the format en-us where EN is the 2 character ISO language code and US is the 2 character ISO country code.
* Product. A product SKU to be translated.
* Project. Like items are grouped into a Project and delivered to the translation system.  Translation completion notifications is done at the project level.  For translation efficiency, all products in project must be in the same top level category and have the same language and quality settings.
* Service.  A service defines pricing and quality level for a project.
* Quote. A quote maps to a transaction which can include 1 or more projects.
