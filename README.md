# Lionbridge onDemand API Docs

## Getting Started

To check out and build this project:

```bash
$ brew install pyenv
$ pyenv install 2.7.13
$ git clone git@bitbucket.org:liox-ondemand/liox-ondemand-api-docs-v1.git
$ cd liox-ondemand-api-docs-v1
$ pyenv local 2.7.13
$ pip install -r requirements.txt
$ make
$ open _build/html/index.html
```
 
